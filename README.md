* deploys traefik in a `traefik` namespace
* deploys https://whoami.cyberrs.xyz in the same namespace
* deploys https://whoamidefault.cyberrs.xyz in namespace `default` to proof traefik is acting as cross namespace ingress controller
